from ctypes import cdll, c_double, POINTER, byref, c_void_p

# make a Python function that wraps 'myfunc'
# (this example demonstrates using output parameters instead
#  of return values, but either would suffice here)
myprog = cdll.LoadLibrary("myprog")
myfunc = myprog['myfunc']
myfunc.argtypes = (c_double, c_double, POINTER(c_double))
myfunc.restype  =  c_void_p

# test wrapped function
result = c_double()
myfunc(3.0, 2.0, byref(result))
assert result.value == 4.0, result.value

# Convenience class for timing
class Timer(object):
    def __enter__(self):
        self._start = time.time()
        return self
    def __exit__(self, exc_type, exc_value, traceback):
        self.duration = time.time() - self._start


import time, argparse
import opentuner
from opentuner.measurement import MeasurementInterface
from opentuner.search.manipulator import *
from opentuner.search.objective import MaximizeAccuracy

class MyTuner(MeasurementInterface):
    def objective(self):
        return MaximizeAccuracy()

    def manipulator(self):
        """ Declare search space. """
        manipulator = ConfigurationManipulator()
        manipulator.add_parameter( FloatParameter("x", -10.0, 10.0) )
        manipulator.add_parameter( FloatParameter("y", -10.0, 10.0) )
        return manipulator

    def run(self, desired_result, input, limit):
        """ Evaluate a configuration. """
        cfg = desired_result.configuration.data
        x, y = cfg['x'], cfg['y']
        print "running", cfg

        accuracy = c_double()
        with Timer() as t:
            myfunc(x, y, byref(accuracy))

        result = dict(time=t.duration, accuracy=accuracy.value)
        print "got result", result
        return opentuner.resultsdb.models.Result(**result)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(parents=opentuner.argparsers())
    args = parser.parse_args()
    MyTuner.main(args)
