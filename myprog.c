#include <stdio.h>

/**
 * Function to tune. By construction it
 * achieves its max value of 4 at (3,2).
 */
void myfunc(double x, double y, double *accuracy) {
    *accuracy = -1.0 * ((x-3)*(x-3) + (y-2)*(y-2)) + 4.0;
}

int main(int argc, char* argv[]) {
    printf("running myprog.c main\n");

    double ans;
    myfunc(2, 3, &ans);
    printf("test 0: %f\n", ans);

    return 0;
}
